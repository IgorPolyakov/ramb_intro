# RambIntro
Simple API on Rails framework
## Main part
- [x] Deadline
- [x] Базовые CRUD операции:
  - [x] Создание;
  - [x] Получение;
  - [x] Изменение;
  - [x] Удаление.
- [x] Получение материала по относительной ссылке (например /api/topics/относительная_ссылка.html).
- [x] Аутентификация по токену в заголовке или запросе
- [x] Фильтрация материалов по параметрам (рубрика, тэг, список ID)
- [x] Сортировка материалов ASC и DESC (по дате публикации, по ID)

## Additional part
- [ ] Удобная для расширения документация API (Swagger, etc), которой будут пользоваться frontend-разработчики
- [x] Рабочий Docker-образ приложения, готовый к деплою
- [ ] Покрытие тестами

# Материал содержит:
* Заголовок *обязателен*
* Анонс
* Обложку (относительную ссылку на обложку, прим: imgs/2018/03/12/314135135.jpg. Загружать обложку не надо)
* Тело материала *обязателен*
* Относительную ссылку (прим: kotiki/persidskie/kak-kormit-kota.html) *уникальна*, *обязательна*
* Рубрику *обязателен*
* Тэги
* Дату публикации *обязателен*

## Running server
### on develop mode
```
rails server
```
### on production mode
```
docker-compose up
```
## Work with API via `curl`
### Get auth `token`
```
curl -v -H "Accept: application/json" -H "Content-type: application/json" POST -d ' {"email": "email","password": "password"}'  http://localhost:3000/auth
```

### Create `material`  
```
curl -v -H "Accept: application/json" -H "Content-type: application/json" -H 'Authorization: #{token}' -X POST -d ' {"materials":{"header_announcement":"header_announcement", "cover":"cover", "body_material":"body_material", "relative_reference":"link", "rubric":"rubric", "date_of_publication":"2018-01-01"}}'  http://localhost:3000/materials
```

### Read `materials`
```
curl -v -H 'Accept: application/json' -H "Content-type: application/json" http://localhost:3000/materials
```
### Update `material`
```
curl -v -H "Accept: application/json" -H "Content-type: application/json" -H 'Authorization: #{token}' -X PATCH -d ' {"materials":{"header_announcement":"somestring", "cover":"somestring", "body_material":"somestring", "relative_reference":"somestring_one", "rubric":"somestring", "date_of_publication":"2013-08-01"}}'  http://localhost:3000/materials/#{relative_reference}
```
### Delete `material`
```
curl -H 'Authorization: #{token}' -X DELETE http://localhost:3000/materials/#{relative_reference}
```
